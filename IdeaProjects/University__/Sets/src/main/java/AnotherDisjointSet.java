public class AnotherDisjointSet {
    private int[] parent;

    private int count;

    public AnotherDisjointSet(int count) {
        parent = new int[count];
        for (int i = 0; i < count; i++) {
            parent[i] = i;
        }
        this.count = count;
    }

    public void union(int p, int q) {

        int t = p;
        int j = q;
        while (t != parent[t]) t = parent[t];
        while (j != parent[j]) j = parent[t];
        if (t != j) {
            System.out.println(p + " " + q);
            parent[t] = j;
        }
        else System.out.println("Same set");

    }

    public int find(int x) {
        return 4;
    }


}
