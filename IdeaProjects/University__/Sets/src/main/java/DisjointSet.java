
public class DisjointSet {


    private int[] parent;

    private int count;

    public DisjointSet(int count) {
        parent = new int[count];
        for (int i = 0; i < count; i++) {
            parent[i] = i;
        }
        this.count = count;
    }



    public void union(int y, int x) {
        if (find(x) == find(y))
            System.out.println("Same set");
        else System.out.println("New connection");

        int yRoot = find(y);
        int xRoot = find(x);

        parent[xRoot] = yRoot;

//        System.out.println("xRoot " + xRoot + " yRoot " + yRoot + " parent "  + parent[xRoot] );

    }

    public int find(int x) {

        if (parent[x] == x)
            return x;
        else {
            return find(parent[x]);
        }
    }

    public void display() {
        for (int i = 0; i < parent.length; i++)
            System.out.print(parent[i] + " ");
    }

    public void displaySets() {
        System.out.println();
        int amount = 0;
        boolean isTrue = false;
        while (amount != count) {
            for (int i = 0; i < this.count; i++) {
                if (parent[i] == amount) {
                    System.out.print(i + " ");
                    isTrue = true;
                }
            }
            if (isTrue)
                System.out.println();
            amount++;
            isTrue = false;
        }
    }

}

