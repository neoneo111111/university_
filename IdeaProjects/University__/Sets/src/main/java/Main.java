import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
//        int count = scanner.nextInt();
        DisjointSet set = new DisjointSet(6);
        set.union(0, 1);
        set.union(1, 2);
        set.union(0, 2);
        set.union(3, 4);

//        AnotherDisjointSet anotherDisjointSet = new AnotherDisjointSet(6);
//         anotherDisjointSet.union(0, 1);
//         anotherDisjointSet.union(1, 2);
//         anotherDisjointSet.union(0, 2);
//         anotherDisjointSet.union(3, 4);


//        set.union(3, 4);
//        set.union(4, 9);
//        set.union(8, 0);
//        set.union(2, 3);
//        set.union(5, 6);
//        set.union(2, 9);
//        set.union(5, 9);
//        set.union(7, 3);
//        set.union(4, 8);
//        set.union(5, 6);
//        set.union(0, 2);
//        set.union(6, 1);

//        for (int i = 0; i < count ; i++) {
//            int element1 = scanner.nextInt();
//            int element2 = scanner.nextInt();
//
//            set.union(element1, element2);
//
//        }
        set.displaySets();
    }
}