public class BarabasiAlbertModel {

    private int n = 31;
    private int[][] matrix = new int[n][n];

    private int count = 2;

    public BarabasiAlbertModel() {
        matrix[0][1] = 1;
        matrix[1][0] = 1;
        addVertex();
    }

    private void addVertex() {

        while (count != n) {
            for (int i = 0; i < matrix.length; i++) {
                int sumDeg = deg();

                int probability = (oneVertexDeg(i) * 100 / sumDeg);
                int random = (int) (1 + (Math.random() * 101 - 1));
                if (random <= probability && i != count) {
                    matrix[i][count] = 1;
                    matrix[count][i] = 1;

                }

            }
            count++;
        }

    }

    public void display() {
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix.length; j++) {
                System.out.print(matrix[i][j] + ", ");
            }
            System.out.println();
        }
    }

    private int oneVertexDeg(int i) {
        int sum = 0;
        for (int j = 0; j < matrix.length; j++)
            sum += matrix[i][j];
        return sum;
    }


    private int deg() {
        int sumDeg = 0;
        for (int i = 0; i < count; i++) {
            for (int j = 0; j < count; j++)
                sumDeg += matrix[i][j];
        }
        return sumDeg;
    }
}
