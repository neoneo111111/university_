package HashMap;

public class HashMap<K, V> {


    private int con = 4;
    private Entry<K, V>[] table = new Entry[con];
    private int[] count = new int[con];
    private int size = 0;


    public void put(K key, V value) {
        if (key == null) return;

        int hash = hash(key);

        if (table[hash] == null) {
            table[hash] = new Entry<K, V>(key, value, null);
            size++;
        } else {

            Entry<K, V> current = table[hash];

            while (current != null) {
                if (current.key.equals(key)) {
                    current.value = value;
                    return;
                }
                current = current.next;
            }
            table[hash] = new Entry<K, V>(key, value, table[hash]);
            count[hash]++;
            size++;

            if (count[hash] == con - 1)
                resize();
        }
    }

    private void resize() {

        con = con * 2;
        Entry<K, V>[] tempTable = new Entry[con];
        for (int i = 0; i < table.length; i++) {
            Entry<K, V> temp = table[i];
            while (temp != null) {
                int hash = hash(temp.key);
                if (tempTable[hash] == null) tempTable[hash] = new Entry<K, V>(temp.key, temp.value, null);
                else {
                    tempTable[hash] = new Entry<K, V>(temp.key, temp.value, tempTable[hash]);
                }
                temp = temp.next;
            }
        }

        int[] countTemp = new int[con];
        count = countTemp;
        table = tempTable;
    }


    public int size() {
        return size;
    }

    private int hash(K key) {

        String newKey = key.toString();
        int sum = 0;
        for (int i = 0; i < newKey.length(); i++) {
            sum += newKey.charAt(i);
        }

        sum ^= (sum >>> 20) ^ (sum >>> 12);
        sum = sum ^ (sum >>> 7) ^ (sum >>> 4);
        return sum & (con - 1);
    }


    public void display() {
        int i = 0;
        while (i < con) {
            System.out.print(i + ":  ");
            Entry<K, V> temp = table[i];
            boolean bool = true;
            if (temp == null) {
                System.out.println(temp);
                bool = false;
            } else while (temp != null) {

                System.out.print(temp.value + "   ");
                temp = temp.next;
            }
            if (bool) System.out.println();
            i++;
        }
    }


    public V get(K key) {
        int hash = hash(key);
        if (table[hash] == null)
            return null;

        else {
            Entry<K, V> temp = table[hash];

            while (temp != null) {
                if (temp.key.equals(key)) {
                    return temp.value;
                }
                temp = temp.next;
            }
            return null;
        }
    }

    public void remove(K key) {

        int hash = hash(key);
        if (table[hash] == null) return;
        else {
            size--;
            if (table[hash].key.equals(key)) {
                table[hash] = table[hash].next;
            } else while (table[hash].next != null) {
                if (table[hash].next.key.equals(key)) {
                    table[hash].next = table[hash].next.next;
                }
                table[hash].next = table[hash].next.next;

            }
        }
    }
}


