package RBTree;


public class RBTree {


    private Node root;
    private Node inRoot;


    public void insert(int element) {
        insertNode(null, this.root, element);
        insertCase1(inRoot);

    }

    private Node insertNode(Node parent, Node root, int element) {
        if (root == null) {
            root = new Node(element);
            root.setLeft(null);
            root.setRight(null);
            root.setParent(parent);
            if (parent != null) {
                if (root.getData() <= parent.getData()) {
                    parent.setLeft(root);
                } else {
                    parent.setRight(root);
                }
            }
            inRoot = root;
        } else if (element <= root.getData()) {
            root.setLeft(insertNode(root, root.getLeft(), element));
        } else {
            root.setRight(insertNode(root, root.getRight(), element));
        }
        return root;
    }

    //current в корне дерева
    private void insertCase1(Node current) {
        if (current.getParent() == null) {
            current.setRed(false);
            this.root = current;
        } else {
            insertCase2(current);
        }
    }

    // Parent текущего узла чёрный
    private void insertCase2(Node current) {
        if (!current.getParent().isRed()) {
            return;
        } else {
            insertCase3(current);
        }
    }

    //parent and uncle красные, перекрашиваем
    private void insertCase3(Node current) {
        Node uncle = uncle(current);
        Node grandparent;
        if (uncle != null && uncle.isRed() && current.getParent().isRed()) {
            current.getParent().setRed(false);
            uncle.setRed(false);
            grandparent = grandparent(current);
            grandparent.setRed(true);
            insertCase1(grandparent);
        } else {
            insertCase4(current);
        }
    }

    //parent красный, uncle чёрный.current правый потомок parent,parent левый потомок grandparent
    private void insertCase4(Node current) {
        Node grandparent = grandparent(current);
        if ((current == current.getParent().getRight()) && (current.getParent() == grandparent.getLeft())) {
            rotateLeft(current.getParent());
            current = current.getLeft();
        } else if ((current == current.getParent().getLeft()) && (current.getParent() == grandparent.getRight())) {
            rotateRight(current.getParent());
            current = current.getRight();
        }
        insertCase5(current);
    }
//parent красный, uncle чёрный. current левый потомок parent,parent левый потомок grandparent
    private void insertCase5(Node current) {
        Node grandparent = grandparent(current);
        current.getParent().setRed(false);
        grandparent.setRed(true);
        if ((current == current.getParent().getLeft()) && (current.getParent() == grandparent.getLeft())) {
            rotateRight(grandparent);
        } else {
            rotateLeft(grandparent);
        }
    }


    private void rotateLeft(Node current) {
        Node temp = current.getRight();
        temp.setParent(current.getParent());
        if (current.getParent() != null) {
            if (current.getParent().getLeft() == current) {
                current.getParent().setLeft(temp);
            } else {
                current.getParent().setRight(temp);
            }
        } else {
            this.root = temp;
        }

        current.setRight(temp.getLeft());
        if (temp.getLeft() != null) {
            temp.getLeft().setParent(current);
        }
        current.setParent(temp);
        temp.setLeft(current);
    }

    private void rotateRight(Node current) {
        Node temp = current.getLeft();
        temp.setParent(current.getParent());
        if (current.getParent() != null) {
            if (current.getParent().getLeft() == current) {
                current.getParent().setLeft(temp);
            } else {
                current.getParent().setRight(temp);
            }
        } else {
            this.root = temp;
        }

        current.setLeft(temp.getRight());
        if (temp.getRight() != null) {
            temp.getRight().setParent(current);
        }

        current.setParent(temp);
        temp.setRight(current);
    }


    public void show() {
        showRec(root, 0);
    }

    private void showRec(Node root, int level) {
        if (root != null) {
            showRec(root.getRight(), level + 1);

            for (int i = 0; i < level; i++) {
                System.out.print("--");
            }

            System.out.println(root.getData());

            showRec(root.getLeft(), level + 1);
        }
    }

    private void inOrder(Node root) {
        if (root != null) {
            inOrder(root.getLeft());
            inOrder(root.getRight());
        }
    }

    public void inOrderPrint() {
        inOrder(this.root);
    }

    private Node grandparent(Node current) {
        if ((current != null) && (current.getParent() != null)) {
            return current.getParent().getParent();
        } else {
            return null;
        }
    }

    private Node uncle(Node current) {
        Node grandparent = grandparent(current);
        if (grandparent == null) {
            return null;
        }
        if (current.getParent() == grandparent.getLeft()) {
            return grandparent.getRight();
        } else {
            return grandparent.getLeft();
        }
    }

}

