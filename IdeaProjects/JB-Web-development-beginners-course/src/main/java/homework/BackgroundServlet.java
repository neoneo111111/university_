package homework;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "BackgroundServlet", urlPatterns = "/homework/background")
public class BackgroundServlet extends HttpServlet {

    public static boolean aBoolean = false;
    private int count = CountCookie.count;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        count++;
        aBoolean = true;
        String cValue = request.getParameter("backcolor");

        Cookie cookie = new Cookie("backgroundColor", cValue);
        response.addCookie(cookie);

        response.sendRedirect("/homework/background.jsp");


    }


    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        getServletContext().getRequestDispatcher("/homework/background.jsp").forward(request, response);
    }
}
