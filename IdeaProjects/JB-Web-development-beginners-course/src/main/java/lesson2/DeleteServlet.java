package lesson2;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "DeleteServlet", urlPatterns = "/lesson2/delete")
public class DeleteServlet extends HttpServlet {

    List<Tweet> tweets = Twitter.tweets;

//    int count = Twitter.count;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        int id = Integer.parseInt(request.getParameter("item.id"));


        int temp = -1;
        for (int i = 0; i <= tweets.size(); i++) {

            if (tweets.get(i).getId() == id) {
                temp = i;
                break;
            }

        }
        tweets.remove(temp);

        request.setAttribute("tweets", tweets);

        getServletContext().getRequestDispatcher("/lesson2/tweet.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setAttribute("tweets", tweets);
        getServletContext().getRequestDispatcher("/lesson2/tweet.jsp").forward(request, response);
    }
}
