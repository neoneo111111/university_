<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page import="homework.CountCookie" %>
<%@ page import="java.util.Locale" %>

<%
    int value = -1;
    int j = 0;

    Cookie[] cookies = request.getCookies();
    if (cookies != null)
        for (int i = 0; i < cookies.length; i++) {

            if (cookies[i].getName().equals("count")) {
                j = i;
                value = Integer.parseInt(cookies[i].getValue());
                break;
            }
        }

    if (value != -1) {
        CountCookie.setCount(value);

        CountCookie.count();
        int temp = CountCookie.getCount();
        cookies[j].setValue(Integer.toString(temp));
        response.addCookie(cookies[j]);
    } else {
        if (CountCookie.aBoolean) CountCookie.setCount(1);
        else
            CountCookie.setCount(0);
        CountCookie.count();
        Cookie cookie1 = new Cookie("count", Integer.toString(CountCookie.getCount()));
        response.addCookie(cookie1);
    }

%>
<html>
<head>
    <title>background</title>
</head>


<body style="background-color:${cookie.backgroundColor.value} ">


<form action="/homework/background" method="post">
    Select the color of the background:

    <input type="color" name="backcolor" value="#fff">

    <input type="submit">
</form>

<br>
<br>
<%
    Locale locale = request.getLocale();
    if (locale.getLanguage().equals("ru")) out.println("Привет");
    else if (locale.getLanguage().equals("en")) out.println("Hello");
    else if (locale.getLanguage().equals("ja")) out.println("こんにちは");
    else if (locale.getLanguage().equals("de")) out.println("Hallo");
%>

<br>
<br>
<br>
<%
    out.println("Количество посещений сайта  " + CountCookie.getCount());
%>
<br>
<br>


</body>
</html>


