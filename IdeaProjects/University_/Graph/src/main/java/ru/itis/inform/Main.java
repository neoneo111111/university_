package ru.itis.inform;

public class Main {


    public static void main(String[] args) {
        final int n = 3;
        GraphMatrixImp graph = new GraphMatrixImp(n);
        for (int i = 0; i < n; i++) {
            graph.addVertex();
        }


        graph.addEdge(0, 2, 440);
        graph.addEdge(0, 1, 1);
        graph.addDirectedEdge(1, 2, 3);


        graph.showGraph();
        System.out.println();
        int[][] matrix = new int[n][n];
        matrix = graph.runFloyd();

        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println( );

        }
        System.out.println("\n" + graph.getSize());
    }
}
