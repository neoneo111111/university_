package ru.itis.inform;

public interface DirectedGraph {
    void addVertex();

    void addDirectedEdge(int vertexA, int vertexB, int weigh);

    void showGraph();


    int[][] runFloyd();
}
