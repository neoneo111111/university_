package ru.itis.inform;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        LinkedList<Human> human = new LinkedList<>();
        HumansReaderWriter humansReaderWriter = new HumansReaderWriter();
        File file = new File("/home/daria/IdeaProjects/University_/HumanSort/src/main/java/ru/itis/inform/Human.txt");
        human = humansReaderWriter.textReader(file);

        HumanSort humanSort = new HumanSort();


        HumanSortArrayList humanSortArrayList = new HumanSortArrayList();
        human = humanSortArrayList.sort(human);
        humansReaderWriter.textWriter("/home/daria/IdeaProjects/University_/HumanSort/src/main/java/ru/itis/inform/SortedHumans.txt", human);
    }

}