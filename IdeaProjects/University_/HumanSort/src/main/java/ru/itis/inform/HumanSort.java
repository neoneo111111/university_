package ru.itis.inform;

public class HumanSort {

    LinkedList<Human> sort(LinkedList<Human> humans) {

        LinkedList<Human>[] humanSort = new LinkedList[100];

        Iterator<Human> iterator = humans.iterator();

        while (iterator.hasNext()) {
            int currentAge = iterator.peekNext().getAge();
            Human currentHuman = iterator.peekNext();

            if (humanSort[currentAge] == null) {
                humanSort[currentAge] = new LinkedList<Human>();
                humanSort[currentAge].add(currentHuman);
            } else {
                humanSort[currentAge].add(currentHuman);
            }
            iterator.next();
        }

        LinkedList<Human> resultList = new LinkedList<Human>();
        resultList.add(null);

        for (int i = 0; i < 100; i++) {
            if (humanSort[i] != null) {
                resultList.append(humanSort[i]);

            }
        }

        return resultList;

    }
}
