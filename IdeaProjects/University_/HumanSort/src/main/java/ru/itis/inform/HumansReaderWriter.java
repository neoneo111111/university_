package ru.itis.inform;

import java.io.*;


import java.util.Scanner;

public class HumansReaderWriter {

    private LinkedList<Human> humans = new LinkedList<Human>();

    public LinkedList<Human> textReader(File file) {

        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNext()) {

                String name = scanner.next() + " ";
                name += scanner.next();
                int age = Integer.parseInt(scanner.next());
                Human newHuman = new Human(name, age);
                humans.add(newHuman);
            }
        } catch (IOException ex) {
            System.out.println("There are no such file");
        }

        return humans;
    }


    public void textWriter(String fileName, LinkedList<Human> list) {

        File file = new File(fileName);


        try (FileWriter writer = new FileWriter(fileName, false)) {


            if (!file.exists()) {

                file.createNewFile();
            }


            Iterator<Human> iterator = list.iterator();
            iterator.next();


            while (iterator.hasNext()) {
                System.out.println(iterator.peekNext().getName() + " " + iterator.peekNext().getAge());
                writer.write(iterator.peekNext().getName() + " " + iterator.peekNext().getAge());

                writer.append('\n');

                writer.flush();
                iterator.next();
            }
        } catch (IOException ex) {

            System.out.println("Error");
        }

    }
}

