package ru.itis.inform;

public class Human implements Comparable<Human> {
    private int age;
    private String name;

    public Human(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }



    @Override
    public int compareTo(Human o) {
     if (this.age > o.getAge()) return 1;
        else if (this.age < o.getAge()) return -1;
        else return 0;

    }
}