package ru.itis.inform;

import ru.itis.inform.ArrayList;
import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

public class HumanSortArrayList {
    LinkedList<Human> sort(LinkedList<Human> humans) {

        int maxAge = 2000;

        ArrayList<LinkedList<Human>> arrayList = new ArrayList<LinkedList<Human>>();

        for (int i = 0; i <= maxAge; i++) {
            arrayList.add(new LinkedList<Human>());
        }

        Iterator<Human> iterator = humans.iterator();

        while (iterator.hasNext()) {
            int currentAge = iterator.peekNext().getAge();
            Human currentHuman = iterator.peekNext();

            if (!arrayList.get(currentAge).isEmpty()) {
                LinkedList<Human> newOne = arrayList.get(currentAge);
                newOne.add(currentHuman);
                arrayList.set(currentAge, newOne);
            } else {
                LinkedList<Human> newOne = arrayList.get(currentAge);
                newOne.addEnd(currentHuman);
                arrayList.set(currentAge, newOne);
            }

            iterator.next();
        }

        LinkedList<Human> resultList = new LinkedList<Human>();

        for (int i = 0; i < maxAge; i++) {
            resultList.append(arrayList.get(i));
        }

        return resultList;

    }
}

