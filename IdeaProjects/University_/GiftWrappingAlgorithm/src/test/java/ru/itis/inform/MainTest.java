package ru.itis.inform;

import org.junit.Test;
import org.omg.PortableServer.POA;

import java.util.ArrayList;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void main() throws Exception {

        Point[] points = {new Point(0, 0), new Point(1, 1), new Point(3, 3), new Point(2, 2), new Point(0, 3), new Point(2, 1), new Point(3, 0)};
        GiftWrappingAlgorithm wrappingAlgorithm = new GiftWrappingAlgorithm();
        ArrayList<Point> actualResult = wrappingAlgorithm.wrap(points);
        ArrayList<Point> expectedResult = new ArrayList<Point>();
        expectedResult.add(0, new Point(0, 0));
        expectedResult.add(1, new Point(3, 0));
        expectedResult.add(2, new Point(3, 3));
        expectedResult.add(3, new Point(0, 3));

        for (int i = 0; i < actualResult.size() - 1; i++) {
            assertEquals(expectedResult.get(i).getX(), actualResult.get(i).getX());
            assertEquals(expectedResult.get(i).getY(), actualResult.get(i).getY());

        }

    }
}