package ru.itis.inform;

public class Point {

    private int x, y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;

    }

    public static Point findMin(Point[] points) {
        Point minPoint = new Point(10000, 10000);
        for (int i = 0; i < points.length; i++) {
            if (points[i].getY() < minPoint.getY()) {
                minPoint.setY(points[i].getY());
                minPoint.setX(points[i].getX());
            } else if (points[i].getX() < minPoint.getX() && points[i].getY() == minPoint.getY()) {
                minPoint.setY(points[i].getY());
                minPoint.setX(points[i].getX());
            }

        }
        return minPoint;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
