package ru.itis.inform;

import java.lang.*;
import java.util.ArrayList;

public class GiftWrappingAlgorithm {


    public ArrayList<Point> wrap(Point[] points) {

        if (points.length < 3) throw new IllegalArgumentException("There are not enough elements");
        Point begin = Point.findMin(points);
        Point check = new Point(begin.getX(), begin.getY());
        Point end = GiftWrappingAlgorithm.findMinPolarCoordinateAngel(points, begin);

        ArrayList<Point> resultPoints = new ArrayList<Point>();
        resultPoints.add(0, new Point(begin.getX(), begin.getY()));
        resultPoints.add(1, new Point(end.getX(), end.getY()));


        int count = 2;
        while (check.getX() != end.getX() || check.getY() != end.getY()) {

            Point temp = new Point(end.getX(), end.getY());

            end = findMaxAngelVector(points, begin, end);
            begin = new Point(temp.getX(), temp.getY());

            resultPoints.add(count, new Point(end.getX(), end.getY()));
            count++;
        }

        return resultPoints;

    }

    //координаты точки
    public static Point findMinPolarCoordinateAngel(Point[] points, Point begin) {
        double min = Double.MAX_VALUE;
        Point minPoint = new Point(0, 0);
        for (int i = 0; i < points.length; i++) {
            if (minPolarCoordinateAngel(points[i].getX(), points[i].getY(), begin) < min && (points[i].getX() != begin.getX() || points[i].getY() != begin.getY())) {
                min = minPolarCoordinateAngel(points[i].getX(), points[i].getY(), begin);
                minPoint = new Point(points[i].getX(), points[i].getY());
            }
        }
        return minPoint;
    }

    //координаты точки
    public static double minPolarCoordinateAngel(int x, int y, Point begin) {

        return Math.atan2(y - begin.getY(), x - begin.getX());
    }


    //координаты вектора, координаты точки
    public static double maxAngelVector(Point begin, Point end, Point point) {

        Point vector1 = new Point(end.getX() - begin.getX(), end.getY() - begin.getY());
        Point vector2 = new Point(point.getX() - end.getX(), point.getY() - end.getY());

        int xy = vector1.getX() * vector2.getX() + vector1.getY() * vector2.getY();

        double sqrt1 = Math.sqrt((Math.pow(vector1.getX(), 2)) + Math.pow(vector1.getY(), 2));

        double sqrt2 = Math.sqrt((Math.pow(vector2.getX(), 2)) + Math.pow(vector2.getY(), 2));

        double angel = xy / (sqrt1 * sqrt2);

        return angel;
    }

    public static Point findMaxAngelVector(Point[] points, Point begin, Point end) {

        double max = -10000000;
        Point maxPoint = new Point(0, 0);
        for (int i = 0; i < points.length; i++) {
            if (maxAngelVector(begin, end, points[i]) > max)

            {

                max = maxAngelVector(begin, end, points[i]);

                maxPoint.setX(points[i].getX());
                maxPoint.setY(points[i].getY());
            }

        }
        return maxPoint;
    }

}
