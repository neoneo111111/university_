import java.util.ArrayList;

public class Algorithm {
    public static void findMinPath(int[][] graph, int first, int last) {


        ArrayList<Visited> arrayList = new ArrayList<>();
        final int INF = Integer.MAX_VALUE;
        for (int i = 0; i < graph.length; i++) {
            arrayList.add(i, new Visited(INF, false));
        }
        arrayList.set(first, new Visited(0, true));

//
//        for (int i = 0; i < graph.length; i++)
//            System.out.println(arrayList.get(i));
        int min = 0;
        int currentVer = first;

        while (min != INF) {

            for (int j = 0; j < graph.length; j++) {

                if (arrayList.get(j).weight > min + graph[currentVer][j] && graph[currentVer][j] != 0) {
                    arrayList.set(j, new Visited(min + graph[currentVer][j], false));
                }

            }

//            for (int i = 0; i < graph.length; i++)
//                System.out.println(arrayList.get(i).weight);


            min = INF;


            for (int j = 0; j < arrayList.size(); j++) {
                if (arrayList.get(j).weight < min && arrayList.get(j).weight != 0 && !arrayList.get(j).isVisited) {
                    currentVer = j;
                    min = arrayList.get(j).weight;
                    arrayList.get(j).setVisited(true);
                }
            }

        }

        if (arrayList.get(last).weight != INF)
            System.out.println(arrayList.get(last).weight);
        else System.out.println("There is no way to that vertex");
    }
}

class Visited {
    public boolean isVisited;
    public int weight;

    public Visited(int weight, boolean visited) {
        this.isVisited = visited;
        this.weight = weight;
    }

    public void setVisited(boolean visited) {
        this.isVisited = visited;
    }


}
