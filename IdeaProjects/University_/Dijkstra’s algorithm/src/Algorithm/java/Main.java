import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[][] graph = new int[3][3];
    for (int i = 0; i < 3; i ++)
        for (int j = 0; j < 3; j ++)
            graph[i][j] = scanner.nextInt();

        Algorithm.findMinPath(graph,0,2);
    }
}
//        0 1 10 1
//        0 0 1 0
//        0 6 0 0
//        0 0 1 0


//
//0 2 10
//0 0 2
//0 0 0