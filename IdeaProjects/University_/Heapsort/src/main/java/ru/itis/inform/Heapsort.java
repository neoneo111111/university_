package ru.itis.inform;

public class Heapsort {


    private static int iteration = 0;


    public static int getIteration() {

        return iteration;
    }

    public static void heapify(int[] array, int size, int pos) {

        while (2 * pos + 1 < size) {
            int t = 2 *pos + 1;
            iteration++;


            //Процедура нормализации
            //подкучи в куче с
            //головой в pos
            if (2 * pos + 2 < size && array[2 * pos + 1] < array[2 * pos + 2]) {

                t = 2 * pos + 2;

            }
            if (array[pos] < array[t]) {

                swap(array, pos, t);

                pos = t;



            } else {
                break;
            }
        }
    }


    public static int[] heapMake(int[] array) {
        iteration = 0;
        iteration++;

//Построение кучи из массива при
        int n = array.length;                    //помощи функции heapify
        for (int i = n - 1; i >= 0; i--) {

            heapify(array, n, i);

        }

        return array;

    }

    public static void sort(int[] array) { //Собственно сама сортировка
        int n = array.length;
        heapMake(array);
for (int i = 0; i < array.length; i++){
    System.out.print(array[i] + " ");

}
        System.out.println();
        while (n > 0) {
            iteration++;

            swap(array, 0, n - 1);
            n--;
            heapify(array,n,0);
        }

    }

    private static void swap(int[] array, int i, int j) { //Меняет местами
        int temp = array[i];                                //элементы с
        array[i] = array[j];                                //индексами i и j
        array[j] = temp;                                    //в массиве array
    }


}


