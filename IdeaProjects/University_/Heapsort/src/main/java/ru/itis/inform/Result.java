package ru.itis.inform;

public class Result {

    private long beforeSort;
    private long afterSort;

    private int iteration;

    public void setIteration(int iteration) {
        this.iteration = iteration;
    }

    public int getIteration() {

        return iteration;
    }

    public void setBeforeSort(long beforeSort) {
        this.beforeSort = beforeSort;
    }

    public void setAfterSort(long afterSort) {
        this.afterSort = afterSort;
    }


    public long getBeforeSort() {
        return beforeSort;
    }

    public long getAfterSort() {
        return afterSort;
    }
}
