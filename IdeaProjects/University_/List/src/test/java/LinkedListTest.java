import org.junit.Test;
import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

import static org.junit.Assert.assertEquals;

public class LinkedListTest {

    @Test
    public void testLinkedList() throws Exception {

        LinkedList<Integer> listOne = new LinkedList<>();
        LinkedList<Integer> listTwo = new LinkedList<>();

        LinkedList<Integer> expectedResult = new LinkedList<>();

        expectedResult.add(2);
        expectedResult.addEnd(4);
        expectedResult.addEnd(6);
        expectedResult.addEnd(8);
        expectedResult.addEnd(10);


        listOne.add(2);
        listOne.add(10);

        listTwo.add(6);
        listTwo.add(4);
        listTwo.addEnd(8);

        listOne = LinkedList.mergeSort(listOne);

        listTwo = LinkedList.mergeSort(listTwo);

        LinkedList<Integer> actualResult = new LinkedList<>();
        actualResult = LinkedList.merge(listOne, listTwo);

        Iterator<Integer> actualIterator = actualResult.iterator();
        Iterator<Integer> expectedIterator = expectedResult.iterator();

        while (expectedIterator.hasNext() && actualIterator.hasNext()){
            assertEquals(expectedIterator.peekNext(), actualIterator.peekNext());
            expectedIterator.next();
            actualIterator.next();
        }

    }
}
