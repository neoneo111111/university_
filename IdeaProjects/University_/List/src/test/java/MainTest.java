package test.java;

import org.junit.Test;
import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

import static org.junit.Assert.assertEquals;

public class MainTest {

    @Test
    public void testMain() throws Exception {

        LinkedList<String> testActualResult = new LinkedList<String>();

        LinkedList<String> testExpectedResult = new LinkedList<String>();


        testActualResult.add("now");
        testActualResult.add("happy");
        testActualResult.add("worry,");
        testActualResult.add("Don't");

        testActualResult.remove("now");

        Iterator<String> iteratorActual = testActualResult.iterator();
        Iterator<String> iteratorExpected = testExpectedResult.iterator();
        iteratorActual.next();
        iteratorActual.next();

        iteratorActual.insert("be");

        testActualResult.showLinkedList();


        testExpectedResult.add("happy");
        testExpectedResult.add("be");
        testExpectedResult.add("worry,");
        testExpectedResult.add("Don't");

        testExpectedResult.showLinkedList();

        while (iteratorExpected.hasNext() && iteratorActual.hasNext()) {
            assertEquals(iteratorExpected.peekNext(), iteratorActual.peekNext());
        }


    }
}