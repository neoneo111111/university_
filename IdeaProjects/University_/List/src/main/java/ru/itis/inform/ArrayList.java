package ru.itis.inform;

public class ArrayList<T> {

    private static Object[] elements;

    private int size = 0;

    private int index = 0;

    private final int defaultSize = 1;

    public ArrayList() {

        this.elements = new Object[defaultSize];
    }

    public ArrayList(int size) {

        this.elements = new Object[size];
    }

    public int size() {
        return this.size;
    }

    public void add() {
        Object[] newOne = new Object[elements.length + 1];

        for (int i = 0; i < elements.length; i++) {
            newOne[i] = elements[i];
        }

        this.elements = newOne;
    }

    public void add(T element) {
        if (index == elements.length) {
            add();
        }

        elements[index] = element;
        this.index++;
        this.size++;
    }

    public void set(int index, T element) {
        this.elements[index] = element;
    }

    public T get(int index) {
        if (index >= size || index < 0) {
            throw new IndexOutOfBoundsException();
        }
        return (T) elements[index];
    }

    public static void showArrayList() {
        for (int i = 0; i < elements.length; i++)
            System.out.print(elements[i] + " ");
    }



}