package ru.itis.inform;

public interface List<T> {

    void add(T element);


    void remove(T element);

    int size();

    void showLinkedList();
}