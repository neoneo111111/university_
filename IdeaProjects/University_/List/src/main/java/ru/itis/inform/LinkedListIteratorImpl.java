package ru.itis.inform;

import java.util.NoSuchElementException;

public class LinkedListIteratorImpl<T> implements Iterator<T> {

    Node<T> current;


    public LinkedListIteratorImpl(Node<T> first) {
        this.current = first;
    }

    @Override
    public boolean hasNext() {

        return current != null;
    }

    @Override
    public boolean hasPrevious() {


        return current.getPrevious() != null;
    }


    public T peekNext() {

        return current.getValue();
    }

    public T peekPrevious() {
        if (current.getPrevious() != null) {
            return current.getPrevious().getValue();
        } else {
            throw new NoSuchElementException();
        }
    }


    public void next() {

        this.current = current.getNext();

    }

    @Override
    public Node getNext() {
        return current;
    }

    public void previous() {

        if (current.getPrevious() != null) {
            current = current.getPrevious();
        } else {
            throw new NoSuchElementException();
        }
    }


    @Override
    public void insert(T element) {

        Node newOne = new Node(element);

        if (current.getPrevious() == null) {

            newOne.setNext(current);
            current.setPrevious(newOne);
            this.current = newOne;
        } else if (current == null) {
            current.setNext(newOne);
            newOne.setPrevious(current);
            current = newOne;
        } else

        {

            newOne.setNext(current);
            newOne.setPrevious(current.getPrevious());
            current.getPrevious().setNext(newOne);
            current.setPrevious(newOne);
        }
    }
}