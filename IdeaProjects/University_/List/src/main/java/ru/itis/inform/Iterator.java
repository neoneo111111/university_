package ru.itis.inform;


public interface Iterator<T> {


    boolean hasNext();

    boolean hasPrevious();


    void next();

    Node getNext();

    void previous();


    T peekNext();

    T peekPrevious();

    void insert(T element);
}