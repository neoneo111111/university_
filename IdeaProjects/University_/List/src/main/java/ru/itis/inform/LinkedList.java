package ru.itis.inform;

import java.util.Locale;

public class LinkedList<T> implements List<T> {


    private Node first;

    private int count;


    public LinkedList() {
        this.first = null;
        this.count = 0;

    }

    public void add(T element) {
        Node newNode = new Node(element);

        if (first == null) {
            this.first = newNode;

            this.count++;
        } else {
            this.first.setPrevious(newNode);
            newNode.setNext(this.first);
            first = newNode;

            this.count++;
        }
    }


    public void remove(T element) {


        if (this.first == null)
            return;

        if (this.first.getValue() == element) {

            this.first = this.first.getNext();
            count--;
            return;
        }

        Node node = this.first;
        while (node != null) {
            if (node.getValue() == element) {

                if (node.getNext() == null) {

                    node.getPrevious().setNext(null);
                    count--;
                    return;

                } else if (node.getPrevious() == null) {
                    node.getNext().setPrevious(null);
                    count--;
                    return;
                } else {

                    node.getPrevious().setNext(node.getNext());
                    node.getNext().setPrevious(node.getPrevious());

                    count--;
                    return;
                }

            }
            node = node.getNext();
        }

    }


    public int size() {
        return this.count;
    }

    @Override
    public void showLinkedList() {
        {
            Node node = this.first;
            while (node != null) {
                System.out.print(node.getValue() + " ");
                node = node.getNext();

            }
            System.out.println();
        }
    }


    public Iterator<T> iterator() {
        return new LinkedListIteratorImpl<T>(this.first);
    }

    public void addEnd(T element) {
        Node newNode = new Node(element);

        if (first == null) {
            this.first = newNode;
            this.count++;

        } else {
            Node node = this.first;
            while (node.getNext() != null) {
                node = node.getNext();
            }

            node.setNext(newNode);
            newNode.setPrevious(node);
            newNode.setNext(null);
            this.count++;

        }

    }

    public void append(LinkedList<T> newList) {
        Node last = this.first;
        Iterator iterator = newList.iterator();

        if (!newList.isEmpty()) {

            if (last == null) {
                this.first = iterator.getNext();
            } else {
                while (last.getNext() != null) {
                    last = last.getNext();
                }
                last.setNext(iterator.getNext());
                iterator.getNext().setPrevious(last);
            }
        }
    }

    public boolean isEmpty() {
        if (this.first == null)
            return true;
        else return false;
    }


    public static <T extends Comparable<T>> LinkedList<T> merge(LinkedList<T> listOne, LinkedList<T> listTwo) {
        Iterator<T> iteratorOne = listOne.iterator();
        Iterator<T> iteratorTwo = listTwo.iterator();
        LinkedList<T> resultList = new LinkedList();

        while (iteratorOne.hasNext() && iteratorTwo.hasNext()) {

            int result = iteratorOne.peekNext().compareTo(iteratorTwo.peekNext());

            if (result < 0) {

                resultList.addEnd(iteratorOne.peekNext());
                iteratorOne.next();
            }

            if (result == 0) {
                resultList.addEnd(iteratorOne.peekNext());
                resultList.addEnd(iteratorTwo.peekNext());
                iteratorOne.next();
                iteratorTwo.next();
            }

            if (result > 0) {
                resultList.addEnd(iteratorTwo.peekNext());
                iteratorTwo.next();
            }
        }

        while (iteratorOne.hasNext()) {
            resultList.addEnd(iteratorOne.peekNext());
            iteratorOne.next();
        }

        while (iteratorTwo.hasNext()) {
            resultList.addEnd(iteratorTwo.peekNext());
            iteratorTwo.next();
        }

        return resultList;

    }

    public static <T extends Comparable<T>> LinkedList<T> mergeSort(LinkedList<T> list) {

        int stackPos = 0;
        Iterator<T> iterator = list.iterator();
        int stackNmber = (int) (Math.log(list.size()) / Math.log(2)) + 1;

        ArrayList<LinkedList<T>> stack = new ArrayList<LinkedList<T>>(stackNmber);

        for (int i = 0; i < stackNmber; i++) {
            stack.add(new LinkedList<T>());
        }

        while (iterator.hasNext()) {
            LinkedList<T> linkedList = new LinkedList<T>();
            linkedList.add(iterator.peekNext());
            stack.set(stackPos, linkedList);
            iterator.next();
            stackPos++;

            while (stackPos > 1 && stack.get(stackPos - 1).size() == stack.get(stackPos - 2).size()) {
                stack.set(stackPos - 2, merge(stack.get(stackPos - 2), stack.get(stackPos - 1)));
                stackPos--;
            }
        }

        while (stackPos > 1) {
            stack.set(stackPos - 2, merge(stack.get(stackPos - 2), stack.get(stackPos - 1)));
            stackPos--;
        }

        list = stack.get(0);

        return list;
    }
}

















