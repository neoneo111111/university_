package ru.itis.inform;

import org.junit.Test;

import java.io.File;

import static org.junit.Assert.*;

public class PolinomTest {

    @Test //!!
    public void main() throws Exception {


        String expectedResult = "-x^2 + x + 5x^(-6) + 23x + 2 + 3x^5";
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));

        String actualResult = polinom.toString();
        assertEquals(expectedResult, actualResult);
    }

    @Test //!!
    public void insert() throws Exception {
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));

        polinom.insert(32, 3);
        String expectedResult = "-x^2 + 32x^3 + x + 5x^(-6) + 23x + 2 + 3x^5";
        String actualResult = polinom.toString();
        assertEquals(expectedResult, actualResult);

    }

    @Test //!!
    public void combine() throws Exception {
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/FileForCombine.txt"));

        polinom.combine();

        String expectedResult = "45x^2 + 2 - 4x^3 - x^7 + 54x^(-2)";
        String actualResult = polinom.toString();
        assertEquals(expectedResult, actualResult);


    }

    @Test //!!
    public void delete() throws Exception {

        String expectedResult = "-x^2 + 5x^(-6) + 2 + 3x^5";
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));

        polinom.delete(1);
        String actualResult = polinom.toString();
        assertEquals(expectedResult, actualResult);

    }

    @Test //!!
    public void sum() throws Exception {
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));

        Polinom polinom1 = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/FileForSum.txt"));
        polinom.sum(polinom1);
        String expectedResult = "5x^(-6) + 2 + 26x + 2x^2 + 3x^3 + 12x^4 + 8x^5";
        String actualResult = polinom.toString();
        assertEquals(expectedResult, actualResult);


    }

    @Test //!!
    public void derivate() throws Exception {
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));

        Polinom polinom1 = polinom.derivate();

        String expectedResult = "-2x + 1 - 30x^(-7) + 23 + 15x^4";
        String actualResult = polinom1.toString();
        assertEquals(expectedResult, actualResult);

    }

    @Test
    public void value() throws Exception {

        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/FileForValue.txt"));
        System.out.println(polinom.toString());
        System.out.println(polinom.value(2));

    }

    @Test //!!
    public void deleteOdd() throws Exception {
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));
        polinom.deleteOdd();
        String actualResult = polinom.toString();
        String expectedResult = "2";
        assertEquals(expectedResult, actualResult);
    }

    @Test //!!
    public void sort() throws Exception {
        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/test/java/ru/itis/inform/filename.txt"));
        polinom.sort();
        String expectedResult = "5x^(-6) + 2 + x + 23x - x^2 + 3x^5";
        String actualResult = polinom.toString();
        assertEquals(expectedResult, actualResult);

    }
}