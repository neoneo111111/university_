package ru.itis.inform;

import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.Scanner;

public class Polinom {
    private LinkedList<Variable> polinomList = new LinkedList<Variable>();

    // !!
    public Polinom(File fileName) {
        try (Scanner scanner = new Scanner(fileName)) {

            while (scanner.hasNext()) {

                int coef = Integer.parseInt(scanner.next());
                int deg = Integer.parseInt(scanner.next());
                Variable newVariable = new Variable(coef, deg);
                polinomList.add(newVariable);
            }
        } catch (IOException ex) {
            System.out.println("There is no such file");
        }

    }

    // !!
    public Polinom(LinkedList<Variable> polinomList) {
        this.polinomList = polinomList;
    }

    // !!
    public String toString() {

        if (polinomList.size() == 0) return "";

        Iterator<Variable> iterator = polinomList.iterator();
        Variable temp = iterator.next();
        String polinom = "";

        if (temp.getCoef() == 0 || temp.getCoef() == 1) polinom += "";
        else if (temp.getCoef() == -1) polinom += "-";
        else polinom += temp.getCoef();

        if (temp.getDeg() == 0 && (temp.getCoef() == 1 || temp.getCoef() == -1)) polinom += temp.getCoef();
        else if (temp.getDeg() == 0) polinom += "";

        else if (temp.getDeg() == 1) polinom += "x";
        else if (temp.getDeg() < 0) polinom += "x^" + "(" + temp.getDeg() + ")";
        else polinom += "x^" + temp.getDeg();


        while (iterator.hasNext()) {
            temp = iterator.next();

            if (temp.getCoef() == 0) polinom += "";
            else if (temp.getCoef() == 1) polinom += " + ";
            else if (temp.getCoef() == -1) polinom += " - ";
            else if (temp.getCoef() < 0) polinom += " - " + Math.abs(temp.getCoef());
            else polinom += " + " + temp.getCoef();


            if (temp.getCoef() == 0) polinom += "";
            else if (temp.getDeg() == 0 && (temp.getCoef() == 1 || temp.getCoef() == -1)) polinom += temp.getCoef();
            else if (temp.getDeg() == 0) polinom += "";
            else if (temp.getDeg() == 1) polinom += "x";
            else if (temp.getDeg() < 0) polinom += "x^" + "(" + temp.getDeg() + ")";
            else polinom += "x^" + temp.getDeg();

        }

        return polinom;
    }

    // !!   вставка монома coef*x^deg в полином
    public void insert(int coef, int deg) {
        if (polinomList.size() == 0) polinomList.add(0, new Variable(coef, deg));
        else {
            int position = 0;
            int minMax = Integer.MIN_VALUE;
            for (int i = 0; i < polinomList.size(); i++) {
                if (polinomList.get(i).getDeg() > minMax && polinomList.get(i).getDeg() <= deg) {
                    minMax = polinomList.get(i).getDeg();
                    position = i;
                }
            }
            polinomList.add(position + 1, new Variable(coef, deg));
        }
    }

    //  !!  приведение подобных членов в многочлене
    public void combine() {

        LinkedList<Variable> resultPolinomList = new LinkedList<>();

        for (int i = 0; i < polinomList.size(); i++) {
            int currentDeg = polinomList.get(i).getDeg();
            int currentCoef = polinomList.get(i).getCoef();


            for (int j = i + 1; j < polinomList.size(); j++) {


                if (currentDeg == polinomList.get(j).getDeg()) {
                    currentCoef += polinomList.get(j).getCoef();
                    polinomList.get(j).setCoef(0);

                }

            }
            if (currentCoef != 0)
                resultPolinomList.add(new Variable(currentCoef, currentDeg));
        }

        polinomList = resultPolinomList;
        Iterator<Variable> iterator = polinomList.iterator();
        while (iterator.hasNext()) {
            Variable temp = iterator.next();
            if (temp.getCoef() == 0) iterator.remove();
        }
    }

    // !!   удалить элемент с данным показателем степени
    public void delete(int deg) {

        Iterator<Variable> iterator = polinomList.iterator();
        while (iterator.hasNext()) {
            Variable temp = iterator.next();
            if (temp.getDeg() == deg) iterator.remove();
        }

    }

    //!!    прибавить  к полиному  полином  p
    public void sum(Polinom p) {

        Polinom temp = p.copyPolinom(p);

        temp.sort();
        temp.combine();
        LinkedList<Variable> sumPolinomList = temp.polinomList;

        temp = new Polinom(polinomList);
        temp.sort();
        temp.combine();
        polinomList = temp.polinomList;

        LinkedList<Variable> resultPolinomList = new LinkedList<>();
        int max = 0;

        if (polinomList.size() > sumPolinomList.size()) {
            max = polinomList.size();
            for (int i = 0; i < max; i++) {

                int currentDeg = polinomList.get(i).getDeg();
                int currentCoef = polinomList.get(i).getCoef();
                int position = -1;

                for (int j = 0; j < sumPolinomList.size(); j++) {
                    if (sumPolinomList.get(j).getDeg() == currentDeg) {
                        currentCoef += sumPolinomList.get(j).getCoef();
                        position = j;
                    }
                }

                if (position != -1) sumPolinomList.remove(position);
                resultPolinomList.add(new Variable(currentCoef, currentDeg));
            }

            for (int i = 0; i < sumPolinomList.size(); i++)
                resultPolinomList.add(sumPolinomList.get(i));

        } else if (polinomList.size() <= sumPolinomList.size()) {
            max = sumPolinomList.size();
            for (int i = 0; i < max; i++) {

                int currentDeg = sumPolinomList.get(i).getDeg();
                int currentCoef = sumPolinomList.get(i).getCoef();
                int position = -1;

                for (int j = 0; j < polinomList.size(); j++) {
                    if (polinomList.get(j).getDeg() == currentDeg) {
                        currentCoef += polinomList.get(j).getCoef();
                        position = j;
                    }
                }

                if (position != -1) polinomList.remove(position);
                resultPolinomList.add(new Variable(currentCoef, currentDeg));
            }

            for (int i = 0; i < polinomList.size(); i++)
                resultPolinomList.add(polinomList.get(i));

        }

        temp = new Polinom(resultPolinomList);
        temp.combine();
        temp.sort();
        polinomList = temp.polinomList;



    }

    // !!    взять производную у полинома
    public Polinom derivate() {

        LinkedList<Variable> deriavate = new LinkedList<>();
        for (int i = 0; i < polinomList.size(); i++) {
            if (polinomList.get(i).getDeg() != 0)
                deriavate.add(new Variable(polinomList.get(i).getCoef() * polinomList.get(i).getDeg(), polinomList.get(i).getDeg() - 1));

        }
        Polinom deriavatePolinom = new Polinom(deriavate);
        return deriavatePolinom;

    }

    // !!   вычислить значение полинома в точке x
    public int value(int x) {


        Polinom temp = new Polinom(polinomList);

        temp.sort();
        temp.combine();
        LinkedList<Variable> tempList = temp.polinomList;

        int result = tempList.get(tempList.size() - 1).getCoef();


        for (int i = 0; i < tempList.get(tempList.size() - 1).getDeg(); i++) {
            if (tempList.get(i).getDeg() != (tempList.get(i + 1).getDeg() - 1)) {
                tempList.add(i + 1, new Variable(0, i + 1));
            }
        }


        for (int i = tempList.size() - 2; i >= 0; i--) {

            result = result * x + tempList.get(i).getCoef();

        }

        polinomList = tempList;
        Iterator<Variable> iterator = polinomList.iterator();
        while (iterator.hasNext()) {
            Variable tempVar = iterator.next();
            if (tempVar.getCoef() == 0) iterator.remove();
        }
        return result;
    }

    //  !!  удалить из списка все элементы с нечетными коэффициентами
    public void deleteOdd() {

        Iterator<Variable> iterator = polinomList.iterator();
        while (iterator.hasNext()) {
            Variable temp = iterator.next();
            if ((temp.getCoef() % 2) != 0) {
                iterator.remove();
            }
        }
    }

    //!! сортирует по возрастанию степеней
    public void sort() {
        LinkedList<Variable> resultPolinomList = new LinkedList<>();
        int n = polinomList.size();
        for (int i = 0; i < n; i++) {
            int min = polinomList.get(0).getDeg();
            int position = 0;

            for (int j = 1; j < polinomList.size(); j++) {
                if (min > polinomList.get(j).getDeg()) {
                    min = polinomList.get(j).getDeg();
                    position = j;
                }

            }
            resultPolinomList.add(i, polinomList.get(position));
            polinomList.remove(position);
        }

        polinomList = resultPolinomList;

    }

    public Polinom copyPolinom(Polinom polinom) {
        LinkedList<Variable> listOne = polinom.polinomList;

        LinkedList<Variable> listTwo = new LinkedList<>();

        for (int i = 0; i < listOne.size(); i++) {
            listTwo.add(i, new Variable(listOne.get(i).getCoef(), listOne.get(i).getDeg()));
        }

        Polinom polinom1 = new Polinom(listTwo);
        return polinom1;

    }
}
