package ru.itis.inform;

public class Variable {
    private int coef;
    private int deg;

    public Variable(int coef, int deg) {
        this.coef = coef;
        this.deg = deg;
    }

    public int getCoef() {
        return coef;
    }

    public int getDeg() {
        return deg;
    }

    public void setCoef(int coef) {
        this.coef = coef;
    }

    public void setDeg(int deg) {
        this.deg = deg;
    }
}
