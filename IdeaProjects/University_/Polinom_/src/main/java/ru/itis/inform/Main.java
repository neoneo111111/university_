package ru.itis.inform;

import java.io.File;
import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {

        Polinom polinom = new Polinom(new File("/home/daria/IdeaProjects/University_/Polinom_/src/main/java/ru/itis/inform/filename.txt"));

        System.out.println("Исходный полином " + polinom.toString());
        polinom.sort();
        System.out.println("Отсортированный полином " + polinom.toString());
        polinom.combine();
        System.out.println("Приведение подобных " + polinom.toString());
        polinom.insert(9, 3);
        System.out.println("Вставка элемента " + polinom.toString());

        polinom.delete(2);
        System.out.println("Удаление элемента с некоторой степенью " + polinom.toString());
        System.out.println("Значение многочлена в точке " + polinom.value(2));
        Polinom polinom1 = polinom.derivate();
        System.out.println("Производная " + polinom1.toString());
        polinom.sum(polinom1);

        System.out.println("Сумма многочлена и его производной " + polinom.toString());


        polinom1.deleteOdd();
        System.out.println("Удаление из производной членов с нечетными коэффициентами " + polinom1.toString());



    }
}
