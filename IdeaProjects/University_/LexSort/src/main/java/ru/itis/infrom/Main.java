package ru.itis.infrom;


import ru.itis.inform.ArrayList;
import ru.itis.inform.LinkedList;
import ru.itis.inform.ReaderWriter;

import java.io.File;

public class Main {
    public static void main(String[] args) {

        LinkedList<String> list = new LinkedList<>();

        File file = new File("/home/daria/IdeaProjects/University_/LexSort/src/main/java/ru/itis/infrom/Words.txt");
        ReaderWriter readerWriter = new ReaderWriter();
        LexSort sort = new LexSort();
        list = readerWriter.read(file);
        list = sort.sort(list);

        readerWriter.textWriter("/home/daria/IdeaProjects/University_/LexSort/src/main/java/ru/itis/infrom/SortedWords.txt", list);


    }
}