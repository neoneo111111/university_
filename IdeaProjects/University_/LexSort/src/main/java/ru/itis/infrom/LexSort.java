package ru.itis.infrom;

import ru.itis.inform.ArrayList;
import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

public class LexSort {
    public LinkedList<String> sort(LinkedList<String> words) {


        Iterator<String> itr = words.iterator();
        int maxSize = itr.peekNext().length();
        int max = 26;


        for (int j = maxSize; j > 0; j--) {

            ArrayList<LinkedList<String>> list = new ArrayList<LinkedList<String>>();

            for (int i = 0; i <= max; i++) {
                list.add(new LinkedList<String>());
            }

            Iterator<String> iterator = words.iterator();

            while (iterator.hasNext()) {
                char currentChar = iterator.peekNext().charAt(j - 1);
                int currentNumber = Character.getNumericValue(currentChar) - 10;
                if (currentNumber < 0 && currentChar > 25) {
                    throw new IllegalArgumentException("Error");
                }
                String currentWord = iterator.peekNext();

                if (list.get(currentNumber).isEmpty()) {
                    LinkedList<String> newOne = list.get(currentNumber);
                    newOne.add(currentWord);
                    list.set(currentNumber, newOne);
                } else {
                    LinkedList<String> newOne = list.get(currentNumber);
                    newOne.addEnd(currentWord);
                    list.set(currentNumber, newOne);
                }

                iterator.next();
            }

            LinkedList<String> sortedWord = new LinkedList<String>();
            for (int i = 0; i < max; i++) {
                sortedWord.append(list.get(i));
            }

            words = sortedWord;
        }

        return words;
    }
}