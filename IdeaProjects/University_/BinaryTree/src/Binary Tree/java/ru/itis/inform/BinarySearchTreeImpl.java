package ru.itis.inform;

import java.util.*;

public class BinarySearchTreeImpl implements BinarySearchTree{

    private Node root;
    private int level;

    private Node insertNode(Node root, int element) {
        if (root == null) {
            root = new Node(element);
            /*
            root.setLeft(null);
            root.setRight(null);
            */
        } else if (element <= root.getData()) {
            root.setLeft(insertNode(root.getLeft(), element));
        } else {
            root.setRight(insertNode(root.getRight(), element));
        }

        return root;
    }

    private void showRec(Node root, int level) {
        if (root != null) {
            showRec(root.getRight(), level + 1);

            for (int i = 0; i < level; i++) {
                System.out.print("---");
            }

            System.out.println(root.getData());

            showRec(root.getLeft(), level + 1);
        }
    }

    public void insert(int element) {
        this.root = insertNode(this.root, element);
    }

    public void show() {
        showRec(root, 0);
    }

    private void inOrder(Node root) {
        if (root != null) {
            inOrder(root.getLeft());
            System.out.print(root.getData() + " ");
            inOrder(root.getRight());
        }
    }

    public void printTree() {
        printTree(root);
    }

    private void getLevel(Node tmpRoot) {

        Queue<Node> currentLevel = new LinkedList<Node>();
        Queue<Node> nextLevel = new LinkedList<Node>();

        currentLevel.add(tmpRoot);
        int level = 0;
        while (!currentLevel.isEmpty()) {
            level++;
            Iterator<Node> iter = currentLevel.iterator();
            while (iter.hasNext()) {
                Node currentNode = iter.next();
                if (currentNode.getLeft() != null) {
                    nextLevel.add(currentNode.getLeft());
                }
                if (currentNode.getRight() != null) {
                    nextLevel.add(currentNode.getRight());
                }

            }

            currentLevel = nextLevel;
            nextLevel = new LinkedList<Node>();

        }
        this.level = level;
    }

    private void printTree(Node tmpRoot) {

        Queue<Node> currentLevel = new LinkedList<Node>();
        Queue<Node> nextLevel = new LinkedList<Node>();

        currentLevel.add(tmpRoot);
        int level = 0;
        while (!currentLevel.isEmpty()) {
            level++;
            Iterator<Node> iter = currentLevel.iterator();
            while (iter.hasNext()) {
                Node currentNode = iter.next();
                if (currentNode.getLeft() != null) {
                    nextLevel.add(currentNode.getLeft());
                }
                if (currentNode.getRight() != null) {
                    nextLevel.add(currentNode.getRight());
                }
                System.out.print(currentNode.getData() + " ");
            }
            System.out.println();
            currentLevel = nextLevel;
            nextLevel = new LinkedList<Node>();

        }
        this.level = level;
    }

    public void inOrderPrint() {
        inOrder(this.root);
    }

    private Integer sumOfTheLevel(Node head, int level) {
        int sum = 0;

        if (level == 0)
            if (head != null)
                return head.getData();
            else return -1;


        if (head.getLeft() != null) sum += sumOfTheLevel(head.getLeft(), level - 1);

        if (head.getRight() != null) sum += sumOfTheLevel(head.getRight(), level - 1);

        return sum;
    }

    public boolean isSumOfEveryLeverBiggerThanSumOfPreviousLevel() {
        ArrayList<Integer> arrayList = sumOfEachLevel();
        boolean isTrue = true;
        for (int i = 0; i < arrayList.size() - 1; i++) {
            if (arrayList.get(i) > arrayList.get(i + 1)) {

                isTrue = false;
                break;
            }
        }
        return isTrue;
    }

    public ArrayList<Integer> sumOfEachLevel() {
        ArrayList<Integer> sums = new ArrayList<Integer>();
        getLevel(root);

        for (int i = 0; i < this.level; i++)
            sums.add(i, sumOfTheLevel(root, i));
        return sums;
    }

    public boolean isBST() {
        return isValid(root, Integer.MIN_VALUE,
                Integer.MAX_VALUE);
    }

    private boolean isValid(Node node, int MIN, int MAX) {
        if (node == null)
            return true;
        if (node.getData() > MIN
                && node.getData() < MAX
                && isValid(node.getLeft(), MIN, node.getData())
                && isValid(node.getRight(), node.getData(), MAX))
            return true;
        else
            return false;
    }

    private Node search(Node root, int element) {
        if (root == null) {
            return null;
        } else if (root.getData() == element) {
            return root;
        } else {
            if (search(root.getRight(), element) == null)
                return search(root.getLeft(), element);
            else
                return search(root.getRight(), element);
        }
    }

    public void change(int nowElement, int newElement) {
        Node need = search(root, nowElement);

        if (need != null)
            need.setData(newElement);
        else throw new NoSuchElementException("There is no such element in this tree");
    }
}