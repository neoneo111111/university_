package ru.itis.infrom;

import java.util.ArrayList;

public class Point {

    private int x, y;


    public Point(int x, int y) {
        this.x = x;
        this.y = y;

    }

    public static Point findLeftPoint(ArrayList<Point> points) {
        Point leftPoint = new Point(Integer.MAX_VALUE, Integer.MAX_VALUE);
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).getX() < leftPoint.getX()) {
                leftPoint = new Point(points.get(i).getX(), points.get(i).getY());


            }
        }
        return leftPoint;
    }

    public static Point findRightPoint(ArrayList<Point> points) {
        Point leftPoint = new Point(Integer.MIN_VALUE, Integer.MIN_VALUE);
        for (int i = 0; i < points.size(); i++) {
            if (points.get(i).getX() > leftPoint.getX()) {
                leftPoint = new Point(points.get(i).getX(), points.get(i).getY());


            }
        }
        return leftPoint;
    }


    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public void setY(int y) {
        this.y = y;
    }
}
