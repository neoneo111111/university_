package ru.itis.infrom;

import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();

        ArrayList<Point> points = new ArrayList<Point>();
        for (int i = 0; i < n; i++) {
            points.add(i, new Point(scanner.nextInt(), scanner.nextInt()));
        }
        Quickhull quickhull = new Quickhull();
        System.out.println();
        points = quickhull.quickhull(points);
        for (int i = 0; i < points.size(); i++) {
            System.out.println(points.get(i).getX() + " " + points.get(i).getY());
        }
    }
}
