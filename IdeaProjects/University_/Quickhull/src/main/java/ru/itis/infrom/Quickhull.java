package ru.itis.infrom;

import java.util.ArrayList;

public class Quickhull {

    public ArrayList<Point> quickhull(ArrayList<Point> points) {
        if (points.size() < 3) throw new IllegalArgumentException("There are not enough elements");

        ArrayList<Point> resultList = new ArrayList<Point>();

        ArrayList<Point> topPointList = new ArrayList<Point>();
        ArrayList<Point> bottomPointList = new ArrayList<Point>();

        Point leftPoint = Point.findLeftPoint(points);
        Point rightPoint = Point.findRightPoint(points);
        resultList.add(leftPoint);
        resultList.add(rightPoint);

        points.remove(leftPoint);
        points.remove(rightPoint);


        for (int i = 0; i < points.size(); i++)

        {

            if (pointLocation(leftPoint, rightPoint, points.get(i)) == -1)

                topPointList.add(points.get(i));

            else if (pointLocation(leftPoint, rightPoint, points.get(i)) == 1)

                bottomPointList.add(points.get(i));

        }
        set(leftPoint, rightPoint, bottomPointList, resultList);

        set(rightPoint, leftPoint, topPointList, resultList);


        return resultList;
    }


    public void set(Point leftPoint, Point rightPoint, ArrayList<Point> points, ArrayList<Point> resultPointList) {


        int insertPosition = resultPointList.indexOf(rightPoint);

        if (points.size() == 0)

            return;

        if (points.size() == 1)

        {

            Point p = points.get(0);

            points.remove(p);

            resultPointList.add(insertPosition, p);


            return;

        }
        int dist = Integer.MIN_VALUE;

        int furthestPoint = -1;

        for (int i = 0; i < points.size(); i++)

        {
            int distance = distance(leftPoint, rightPoint, points.get(i));

            if (distance > dist)

            {
                dist = distance;
                furthestPoint = i;
            }

        }

        Point P = points.get(furthestPoint);

        points.remove(furthestPoint);

        resultPointList.add(insertPosition, P);


        ArrayList<Point> topSet = new ArrayList<Point>();

        for (int i = 0; i < points.size(); i++)

        {

            Point M = points.get(i);

            if (pointLocation(leftPoint, P, M) == 1)

            {

                topSet.add(M);

            }

        }

        ArrayList<Point> bottomSet = new ArrayList<Point>();

        for (int i = 0; i < points.size(); i++)

        {

            Point M = points.get(i);

            if (pointLocation(P, rightPoint, M) == 1)

            {

                bottomSet.add(M);

            }

        }

        set(leftPoint, P, topSet, resultPointList);

        set(P, rightPoint, bottomSet, resultPointList);


    }


    public int pointLocation(Point left, Point right, Point point) {


        int position = (right.getX() - left.getX()) * (point.getY() - left.getY()) - (right.getY() - left.getY()) * (point.getX() - left.getX());

        if (position > 0)

            return 1;

        else if (position == 0)

            return 0;

        else

            return -1;


    }


    public int distance(Point leftPoint, Point rightPoint, Point point) {

        int ABx = rightPoint.getX() - leftPoint.getX();

        int ABy = rightPoint.getY() - leftPoint.getY();

        int num = ABx * (leftPoint.getY() - point.getY()) - ABy * (leftPoint.getX() - point.getX());


        return Math.abs(num);

    }


}
