package ru.itis.inform;

import org.junit.Test;
import ru.itis.infrom.Point;
import ru.itis.infrom.Quickhull;


import java.util.ArrayList;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void main() throws Exception {
        ArrayList<Point> points = new ArrayList<Point>();
        points.add(new Point(0, 0));
        points.add(new Point(1, 1));
        points.add(new Point(3, 3));
        points.add(new Point(2, 2));
        points.add(new Point(0, 3));
        points.add(new Point(2, 1));
        points.add(new Point(3, 0));


        Quickhull quickhull = new Quickhull();
        ArrayList<Point> actualResult = quickhull.quickhull(points);
        ArrayList<Point> expectedResult = new ArrayList<Point>();
        expectedResult.add(new Point(3, 0));
        expectedResult.add(1,new Point(0, 0));
        expectedResult.add(2, new Point(0, 3));


        expectedResult.add(3, new Point(3, 3));

        for (int i = 0; i < actualResult.size(); i++)
            System.out.println(actualResult.get(i).getX() + " " + actualResult.get(i).getY());


        for (int i = 0; i < actualResult.size() - 1; i++) {
            assertEquals(expectedResult.get(i).getX(), actualResult.get(i).getX());
            assertEquals(expectedResult.get(i).getY(), actualResult.get(i).getY());

        }

    }
}