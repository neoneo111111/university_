package ru.itis.inform.arreyFarey;

public class FareySequenceGeneratorArrayImpl {

    public FareySequenceGeneratorArrayImpl() {
    }

    public RationalNumber[] generate(int n) {

        RationalNumber[] rationalNumbers = new RationalNumber[n * n];

        rationalNumbers[0] = new RationalNumber(0, 1);
        rationalNumbers[1] = new RationalNumber(1, 1);

        for (int i = 2; i < n * n; i++) {
            rationalNumbers[i] = null;
        }

        int k = 2;
        int j1;
        int j2;

        for (int i = 2; i <= n; i++)
            for (int j = 0; j <= k - 2; j++) {

                if (rationalNumbers[j] != null && rationalNumbers[j + 1] != null) {

                    j2 = rationalNumbers[j].getB();
                    j1 = rationalNumbers[j + 1].getB();

                    if (j2 + j1 == i) {
                        k++;

                        RationalNumber newOne = new RationalNumber((rationalNumbers[j].getA() + rationalNumbers[j + 1].getA()), (rationalNumbers[j].getB() + rationalNumbers[j + 1].getB()));
                        for (int l = k; l > j; l--) {
                            rationalNumbers[l] = rationalNumbers[l - 1];
                        }
                        rationalNumbers[j + 1] = newOne;
                    }
                }
            }
        return rationalNumbers;
    }

}
