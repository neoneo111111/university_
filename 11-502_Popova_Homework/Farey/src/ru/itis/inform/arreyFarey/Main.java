package ru.itis.inform.arreyFarey;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        FareySequenceGeneratorArrayImpl array = new FareySequenceGeneratorArrayImpl();
        RationalNumber numbers[] = array.generate(n);

        int j = 0;
        while (numbers[j + 1] != null) {
            System.out.println(numbers[j].getA() + "/" + numbers[j].getB() + " ");
            j++;
        }

        System.out.println(numbers[j].getA() + "/" + numbers[j].getB() + "");
    }
}

