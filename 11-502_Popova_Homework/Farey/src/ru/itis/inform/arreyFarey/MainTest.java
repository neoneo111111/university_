package ru.itis.inform.arreyFarey;

import static org.junit.Assert.assertEquals;

public class MainTest {

    @org.testng.annotations.Test
    public void testMain() throws Exception {

        final int n = 2;
        RationalNumber[] expectedMatrix = new RationalNumber[n * n];

        FareySequenceGeneratorArrayImpl array = new FareySequenceGeneratorArrayImpl();
        RationalNumber actualMatrix[] = array.generate(n);
        expectedMatrix[0] = new RationalNumber(0, 1);
        expectedMatrix[1] = new RationalNumber(1, 2);
        expectedMatrix[2] = new RationalNumber(1, 1);

        for (int i = 0; i < n; i++) {
            assertEquals(expectedMatrix[i].getA(), actualMatrix[i].getA());
            assertEquals(expectedMatrix[i].getB(), actualMatrix[i].getB());
        }


    }
}