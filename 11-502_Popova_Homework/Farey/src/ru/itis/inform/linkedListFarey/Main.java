package ru.itis.inform.linkedListFarey;

import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        Farey farey = new Farey();

        LinkedList<RationalNumber> list = farey.runFarey(n);

        Iterator<RationalNumber> itr = list.iterator();
        while (itr.hasNext()) {
            System.out.println(itr.peekNext().getA() + "/" + itr.peekNext().getB());
            itr.next();
        }


    }

}
