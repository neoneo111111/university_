package ru.itis.inform.linkedListFarey;


import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

public class Farey {

    LinkedList<RationalNumber> runFarey(int n) {

        LinkedList<RationalNumber> fareyGenerator = new LinkedList<>();

        fareyGenerator.add(new RationalNumber(1, 1));
        fareyGenerator.add(new RationalNumber(0, 1));

        for (int i = 2; i <= n; i++) {


            Iterator<RationalNumber> iterator = fareyGenerator.iterator();
            iterator.next();

            while (iterator.hasNext()) {

                if (iterator.peekPrevious().getB() + iterator.peekNext().getB() == i) {

                    RationalNumber newOne = new RationalNumber(iterator.peekPrevious().getA() + iterator.peekNext().getA(), iterator.peekPrevious().getB() + iterator.peekNext().getB());

                    iterator.insert(newOne);

                }

                iterator.next();

            }


        }

        return fareyGenerator;
    }

}