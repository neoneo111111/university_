package ru.itis.inform.linkedListFarey;

public class RationalNumber {


    private int a, b;

    public RationalNumber(int a, int b) {
        this.a = a;

        if (b != 0) this.b = b;
        else throw new IllegalArgumentException();
    }

    public int getA() {
        return a;
    }

    public int getB() {
        return b;
    }
}
