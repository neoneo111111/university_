package ru.itis.inform.linkedListFarey;

import org.junit.Test;
import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

import static org.junit.Assert.assertEquals;

public class MainTest {

    @Test
    public void testMain() throws Exception {
        Farey farey = new Farey();
        LinkedList<RationalNumber> testActual = farey.runFarey(2);
        Iterator<RationalNumber> itrActual = testActual.iterator();


        LinkedList<RationalNumber> testExpected = new LinkedList<>();
        Iterator<RationalNumber> itrExpected = testExpected.iterator();
        testExpected.add(new RationalNumber(0, 1));
        testExpected.add(new RationalNumber(1, 2));
        testExpected.add(new RationalNumber(1, 1));


        while (itrActual.hasNext() && itrExpected.hasNext()) {
            assertEquals(itrExpected.peekNext().getA(), itrActual.peekNext().getA());
            assertEquals(itrExpected.peekNext().getB(), itrActual.peekNext().getB());
        }
    }
}