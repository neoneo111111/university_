package ru.itis.inform;

public class DirectedGraph1 implements DirectedGraph{
    private static final int size = 50;
    private int [][] matrix;
    private int maxSize;
    private int verticesCount;

    private void initGraph(int maxSize){
        this.maxSize = maxSize;
        this.verticesCount = 0;
        this.matrix = new int [maxSize][maxSize];
    }

    public DirectedGraph1(int maxSize) {
        initGraph(maxSize);
    }

    public DirectedGraph1() {
        initGraph(size);
    }


    @Override
    public void addVertex() {
        if (this.verticesCount < maxSize)
            this.verticesCount++;
        else throw new IllegalArgumentException();

    }

    @Override
    public void addEdge(int vertexA, int vertexB, int weigh) {
        if (vertexA < verticesCount && vertexB < verticesCount)
            this.matrix[vertexA][vertexB] = weigh;

    }

    @Override
    public void showGrap() {
        for (int i = 0; i < verticesCount; i++) {
            for (int j = 0; j < verticesCount - 1; j++) {
                System.out.print(matrix[i][j] + ",  ");
            }
            System.out.println(matrix[i][verticesCount - 1]);
        }

    }
}
