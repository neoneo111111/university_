package ru.itis.inform;

public interface Graph {
    void addVertex();

    void addEdge(int vertexA, int vertexB, int weigh);

    void showGraph();

    int[][] runFloyd();
}
