package ru.itis.inform;

public class GraphMatrixImp implements DirectedGraph, Graph {


    private static final int size = 50;
    private int[][] matrix;
    private int maxSize;
    private int verticesCount;


    public GraphMatrixImp(int maxSize) {
        initGraph(maxSize);
    }

    public GraphMatrixImp() {
        initGraph(size);
    }


    private void initGraph(int maxSize) {
        this.maxSize = maxSize;
        this.verticesCount = 0;
        this.matrix = new int[maxSize][maxSize];
    }

    public void addVertex() {
        if (this.verticesCount < maxSize)
            this.verticesCount++;
        else throw new IllegalArgumentException();
    }


    public void addEdge(int vertexA, int vertexB, int weigh) {
        if (vertexA < verticesCount && vertexB < verticesCount)
            this.matrix[vertexA][vertexB] = weigh;
        this.matrix[vertexB][vertexA] = weigh;
    }


    public void addDirectedEdge(int vertexA, int vertexB, int weigh) {
        if (vertexA < verticesCount && vertexB < verticesCount)
            this.matrix[vertexA][vertexB] = weigh;
        this.matrix[vertexB][vertexA] = -1;
    }


    public void showGraph() {
        for (int i = 0; i < verticesCount; i++) {
            for (int j = 0; j < verticesCount - 1; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println(matrix[i][verticesCount - 1]);
        }
    }


    public int[][] runFloyd() {


        for (int k = 0; k < this.verticesCount; k++) {
            for (int i = 0; i < verticesCount; i++) {
                for (int j = 0; j < verticesCount; j++) {
                    if (this.matrix[i][j] != -1 && this.matrix[i][k] != -1 && this.matrix[k][j] != -1)
                        this.matrix[i][j] = Math.min(this.matrix[i][j], this.matrix[i][k] + this.matrix[k][j]);
                }
            }
        }


        return this.matrix;
    }
}
