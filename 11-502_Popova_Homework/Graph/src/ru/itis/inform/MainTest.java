package ru.itis.inform;


import static org.junit.Assert.assertArrayEquals;

public class MainTest {


    @org.junit.Test
    public void testMain() throws Exception {
        final int n = 3;
        GraphMatrixImp tester = new GraphMatrixImp(n);

        for (int i = 0; i < n; i++)
            tester.addVertex();
        tester.addEdge(0, 2, 440);
        tester.addEdge(0, 1, 1);
        tester.addDirectedEdge(1, 2, 3);


        int[][] expextedResult = {{0, 1, 4}, {1, 0, 3}, {440, -1, 0}};
        int[][] actualResult = tester.runFloyd();

        assertArrayEquals(expextedResult, actualResult);
    }
}