package ru.itis.inform;

public class Human {
    private int age;
    private String name;

    public Human(String name, int age) {
        this.name = name;
        this.age = age;

    }

    public int getAge() {
        return age;
    }

    public String getName() {
        return name;
    }

}