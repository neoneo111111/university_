package ru.itis.inform;

import org.junit.Test;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void testMain() throws Exception {

        LinkedList<Human> expectedList = new LinkedList<>();
        expectedList.add(new Human("Harry Osborn", 23));
        expectedList.addEnd(new Human("Mary-Jane Watson", 24));
        expectedList.addEnd(new Human("Peter Parker", 25));
        expectedList.addEnd(new Human("Aunt May", 57));
        expectedList.addEnd(new Human("Uncle Ben", 66));


        LinkedList<Human> actualList = new LinkedList<>();
        actualList.add(new Human("Peter Parker", 25));
        actualList.add(new Human("Uncle Ben", 66));
        actualList.add(new Human("Harry Osborn", 23));
        actualList.add(new Human("Aunt May", 57));
        actualList.add(new Human("Mary-Jane Watson", 24));

   HumanSortArrayList humanSortArrayList = new HumanSortArrayList();
        actualList = humanSortArrayList.sort(actualList);
        Iterator<Human> expectedIterator = expectedList.iterator();
        Iterator<Human>  actualIterator = actualList.iterator();
        while (expectedIterator.hasNext() && actualIterator.hasNext())
        {
            assertEquals(expectedIterator.peekNext().getAge(), actualIterator.peekNext().getAge());

            actualIterator.next();
            expectedIterator.next();
        }

    }
}