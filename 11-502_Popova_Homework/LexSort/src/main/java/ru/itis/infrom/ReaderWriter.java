package ru.itis.inform;

import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

import java.io.*;
import java.util.Scanner;

public class ReaderWriter {

    private LinkedList<String> list = new LinkedList<String>();

    public LinkedList<String> read(File file) {
        try (Scanner scanner = new Scanner(file)) {

            while (scanner.hasNext()) {

                String word = scanner.nextLine();

                list.add(word);

            }
        } catch (IOException ex) {
            System.out.print("No such file");
        }


        return list;
    }


    public void textWriter(String fileName, LinkedList<String> list) {
        Iterator<String> iterator = list.iterator();
        File file = new File(fileName);


        try (FileWriter writer = new FileWriter(fileName, false)) {


            if (!file.exists()) {

                file.createNewFile();
            }


            while (iterator.hasNext()) {
                System.out.println(iterator.peekNext());
                writer.write(iterator.peekNext());

                writer.append('\n');

                writer.flush();
                iterator.next();
            }
        } catch (IOException ex) {

            System.out.println("Error");
        }

    }
}



