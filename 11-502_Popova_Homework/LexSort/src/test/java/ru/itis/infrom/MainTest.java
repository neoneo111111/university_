package ru.itis.infrom;

import org.junit.Test;
import ru.itis.inform.Iterator;
import ru.itis.inform.LinkedList;

import static org.junit.Assert.*;

public class MainTest {

    @Test
    public void testMain() throws Exception {


        LinkedList<String>  actualResult = new LinkedList<>();
        LinkedList<String> expectedResult = new LinkedList<>();

        actualResult.add("fdas");
        actualResult.add("oiuf");
        actualResult.add("pofd");
        actualResult.add("ufds");
        actualResult.add("ffas");

        expectedResult.add("fdas");
        expectedResult.addEnd("ffas");
        expectedResult.addEnd("oiuf");
        expectedResult.addEnd("pofd");
        expectedResult.addEnd("ufds");

        LexSort lexSort = new LexSort();
        actualResult = lexSort.sort(actualResult);
        Iterator<String> expectedIterator = expectedResult.iterator();
        Iterator<String> actualIterator = actualResult.iterator();

        while (expectedIterator.hasNext() && actualIterator.hasNext()){
            assertEquals(expectedIterator.peekNext(), actualIterator.peekNext());
            expectedIterator.next();
            actualIterator.next();
        }


    }
}