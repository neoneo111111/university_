package com.jetbrains;

import javax.tools.Tool;
import java.awt.*;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileFilter;
import java.io.FilenameFilter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.util.Timer;
import java.util.TimerTask;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DirectoryScanner {

    Scanner scanner = new Scanner(System.in);

    private File inputDir;
    private File outputDir;
    private Pattern mask;
    private long waitInterval;
    private boolean includeSubfolders;
    private boolean autoDelete;
    private FilenameFilter textFilter;

    public DirectoryScanner(String inputDir, String outputDir, String mask, long waitInterval, boolean includeSubfolders, boolean autoDelete) {
        this.inputDir = new File(inputDir);

        Path path = Paths.get(outputDir);
        try {
            Files.createDirectory(path);
            this.outputDir = new File(outputDir);
        } catch (Exception e) {
            this.outputDir = new File(outputDir);
        }
        this.mask = Pattern.compile(makeMask(mask));
        this.waitInterval = waitInterval;
        this.includeSubfolders = includeSubfolders;
        this.autoDelete = autoDelete;
    }


    public void scan() {
        Timer timer = new Timer();
        TimerTask myTask = new TimerTask() {
            @Override
            public void run() {
                scan(inputDir, outputDir);
            }
        };

        timer.schedule(myTask, 1,waitInterval * 1000);
//        scan(inputDir, outputDir);
    }

    private void scan(File inputFile, File outputFile) {
        File[] fileList = inputFile.listFiles();

        for (int i = 0; i < fileList.length; i++) {

            if (fileList[i].isDirectory() && includeSubfolders) {

//                System.out.println(fileList[i].getName()  + " " + outputFile.getAbsolutePath());
                String newOutputFileName = outputFile.getAbsolutePath() + "/" + fileList[i].getName();
                Path path = Paths.get(newOutputFileName);
                if (!Files.exists(path)) {
                    try {
                        Files.createDirectory(path);

                    } catch (Exception e) {
                        System.out.println("Couldn't create a directory private scan()/  if (fileList[i].isDirectory() && includeSubfolders)");
                    }

                    File newOutputFile = new File(newOutputFileName);

                    scan(fileList[i], newOutputFile);
                }

            } else {
                if (regularExpression(fileList[i].getName())) {
                    try {
                        Path inputPath = Paths.get(inputFile.getAbsolutePath() + "/" + fileList[i].getName());

                        Path outputPath = Paths.get(outputFile.getAbsolutePath() + "/" + fileList[i].getName());

                        if (Files.exists(outputPath)) {
                            Files.delete(outputPath);
                        }
                        Files.copy(inputPath, outputPath);
                        if (autoDelete == true) fileList[i].delete();

                    } catch (Exception e) {
                        System.out.println("Error in privvate scan()/ if (regularExpression(fileList[i].getName()))");
                    }
                }
            }
        }

    }

    private boolean regularExpression(String fileName) {
        Matcher matcher = mask.matcher(fileName);
        return matcher.matches();
    }

    private String makeMask(String mask) {
        String correctMask = "";

        for (int i = 0; i < mask.length(); i++) {
            char temp = mask.charAt(i);
            if (temp == '?') correctMask += ".?";
            else if (temp == '*') correctMask += ".*";
            else if (temp == '.') correctMask += "\\.";
            else correctMask += temp;
        }

        return correctMask;
    }
}
